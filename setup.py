from distutils.core import setup
import os

exec(open('sams_classes/version.py').read())

setup(
  name = 'sams_classes',
  packages = ['sams_classes'],
  version = __version__,
  description = 'SAMS helper classes for Self-(Web)services',
  author = 'Sebastian Lobinger',
  classifiers = [
    'Programming Language :: Python :: 3:: Only',
    'Operating System :: OS Independent',
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'License :: Other/Proprietary License',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: CGI Tools/Libraries',
    'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
    'Topic :: Internet :: WWW/HTTP :: Session',
    'Topic :: Internet :: WWW/HTTP :: WSGI :: Middleware'
    'Topic :: Software Development :: Libraries :: Python Modules'
  ]
)