from flask import Blueprint
from importlib import import_module
from .exceptions import (
  ManifestDefaultLanguageMissing
  , DefaultLanguageDictMissing
  , FunctionNotExists
)
from .sams_proxy import SAMSProxy
import sys, copy

def eprint(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

class SAMSApp:

  def __init__(self, name, manifest = {} , langDict = {}, urlPrefix = ''):
    self.__name = name
    self.__blueprint = Blueprint(name, name, template_folder='templates',
      static_folder = 'static')
    self.__manifest = manifest
    self.__urlPrefix = urlPrefix
    if not self.manifest.get('default_language'):
      raise ManifestDefaultLanguageMissing()
    self.langDict = langDict
    if not isinstance(self.langDict.get(self.manifest.get('default_language'))
        , dict):
      raise DefaultLanguageDictMissing()
    self.__add_urls()
    self.__add_proxy_urls()
  
  @property
  def module(self):
    return import_module(self.__name)
  
  @property
  def name(self):
    return self.__name
  
  def lang(self, langCode: str) -> dict:
    request_lang = self.langDict.get(langCode, {})
    lang = copy.deepcopy(self.langDict[self.__manifest['default_language']])
    lang.update(request_lang)
    return lang
  
  def __add_urls(self):
    for view in self.manifest.get('views', []):
      endpoint = '_'.join(view['function'].split('.'))
      view_func = SAMSApp._get_attr(self.module, view['function'])
      self.__blueprint.add_url_rule(
        rule = self._generate_url(urlPart = view.get('url')), endpoint = endpoint,
        view_func = view_func,
        methods = view.get('methods', ['GET', 'HEAD', 'OPTIONS']))
  
  def __add_proxy_urls(self):
    i = 0
    for proxy in self.proxies:
      self.__blueprint.add_url_rule(
        rule = self._generate_url(urlPart = proxy.urlRule),
        endpoint = self.__blueprint.name.replace('.', '_') + '_proxy_' + str(i),
        view_func = proxy.proxy,
        methods = ['GET', 'POST', 'DELETE', 'UPDATE','PATCH', 'OPTIONS', 'PUT',
          'HEAD'])
      self.__blueprint.add_url_rule(
        rule = self._generate_url(urlPart = proxy.rootUrlRule),
        endpoint = self.__blueprint.name.replace('.', '_') + '_proxy_' + str(i),
        view_func = proxy.proxy,
        methods = ['GET', 'POST', 'DELETE', 'UPDATE','PATCH', 'OPTIONS', 'PUT',
          'HEAD'])
      i += 1
  
  @staticmethod
  def _get_attr(thing, attrString):
    try:
      return getattr(thing, attrString)
    except AttributeError:
      pass
    attrList = attrString.split('.')
    if len(attrList) <= 1:
      raise FunctionNotExists(('The function ' + attrString + ' does not exist'))
    return SAMSApp._get_attr(getattr(thing, attrList[0]), '.'.join(attrList[1:]))
  
  @property
  def blueprint(self):
    return self.__blueprint
  
  @property
  def manifest(self):
    return self.__manifest
  
  @property
  def proxies(self):
    proxies = []
    for definition in self.manifest.get('proxies', []):
      proxies.append(SAMSProxy(definition))
    return proxies
  
  def menu(self, langCode: str, urlPrefix: str = '/') -> list:
    return self._get_men_entries(
      langCode = langCode, menuPattern = self.manifest.get('menu', []),
      urlPrefix = urlPrefix)
  
  def _get_men_entries(
      self, langCode: str, urlPrefix: str, menuPattern: list = []) -> list:
    entries = []
    for element in menuPattern:
      entry = {
        'url': self._generate_url(
          urlPart = element['url'], urlPrefix = urlPrefix,
          external = element.get('external', False)
        ),
        'name': self.lang(langCode)[element['name_string']]}
      if element.get('menu'):
        entry['menu'] = self._get_men_entries(
          langCode = langCode, menuPattern = element['menu'],
          urlPrefix = urlPrefix)
      entries.append(entry)
    return entries
  
  def _generate_url(
      self, urlPrefix: str = '', urlPart: str = '', external = False ) -> str:
    if external:
      return urlPart
    urlPrefix = urlPrefix.strip('/')
    urlPart = urlPart.lstrip('/')
    url = '/'.join([urlPrefix, urlPart])
    if url[0] != '/':
      url = '/' + url
    return url