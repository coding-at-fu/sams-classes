class HubDirNotExist(Exception):
  """Raised when a the hubDir for SAMSHub does not exist
  """
  pass
class FunctionNotExists(Exception):
  """Raised when a function in the module manifest.yaml does not exit
  """
  pass
class ManifestDefaultLanguageMissing(Exception):
  """Raised when the SAMSApp.manifest has not default_language entry set.
  """
class DefaultLanguageDictMissing(Exception):
  """Raised when the SAMSApp.langDict has no subset for the specified default
  language e.g. SAMSApp.langDict.get(SAMSApp.manifest['default_language'])
  is None
  """
class AppNotExist(Exception):
  """Raised when a app called in SAMSHub does not exist
  """
class ProxySpecKeyMssing(Exception):
  """Raised when a mandatory Spec in a SAMSProxy Specification is missing """
class ProxySpecMissing(Exception):
  """Raised when the mandatory PrpxySpec in for SAMSProxy is completely missing
  """ 