import yaml, os, glob
from sams_classes import SAMSHub, SAMSApp
from flask import Flask

def create_hub():
  with open('ExampleApp/config/config.yaml', 'r') as f:
    hubConf = yaml.load(f.read())
  hub = SAMSHub(name= __name__, config=hubConf['exampleApp'])
  hub.flaskApp.config.update(hubConf.get('flask', {}))
  for appEntry in hubConf['apps']:
    appName = appEntry['name']
    appAlias = appEntry.get('alias')
    with open(appName.replace('.', '/') + '/manifest.yaml') as f:
      manifest = yaml.load(f.read())
    langDict = {'en': {}}
    for yaml_file in search_yaml_files(appName.replace('.', '/') + '/lang/'):
      with open(yaml_file) as f:
        singleLang = yaml.load(f.read())
      langDict.update(
        {os.path.splitext(os.path.basename(yaml_file))[0]: singleLang})
    hub.addApp(
        SAMSApp(name = appName, manifest = manifest, langDict = langDict),
        alias = appAlias)
  return hub

def search_yaml_files(path, exts = ['.yaml', '.yml']):
  matchingFiles = []
  for ext in exts:
    for path in glob.glob(path + '*' + ext):
      if os.path.isfile(path):
        matchingFiles.append(path)
  return matchingFiles

if __name__ == '__main__':
  create_hub()
else:
  hub = create_hub()