from flask import render_template, session, request

def home():
  print(session.get('language'))
  return render_template('home.html')

def change_lang():
  if request.args.get('lang'):
    session['language'] = request.args.get('lang')
  print('session language: {0}, request lang {1}'.format(
    session.get('language', 'None'), request.args.get('lang', 'None')) )
  return render_template('change_language.html')