from flask import Flask, request
import flask
import json, sys
""" this is the test api for tdd. All api szenarios are here! """

def eprint(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

app = Flask(__name__)

@app.route('/hello',
  methods=['GET', 'POST', 'DELETE', 'UPDATE', 'PATCH', 'OPTIONS', 'PUT', 'HEAD'])
def hello():
  return request.method.lower() + ' hello'

@app.route('/passthrough',
  methods=['GET', 'POST', 'DELETE', 'UPDATE', 'PATCH', 'OPTIONS', 'PUT', 'HEAD'])
def passthrough():
  body = request.form
  if request.is_json:
    body = request.get_json()
  output = json.dumps(
    {'url': request.args, 'body': body, 'header': dict(request.headers)})
  return output

@app.route('/', methods = [
  'GET', 'POST', 'DELETE', 'UPDATE', 'PATCH', 'OPTIONS', 'PUT', 'HEAD'])
def rootpath():
  return 'rootpath'