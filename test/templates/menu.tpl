<ul>
{% for entry in menu recursive%}
<li><a href='{{ entry.url }}'>{{ entry.name }}</a>
{% if entry.menu %}
<ul>
{{ loop(entry.menu) }}
</ul>
{% endif %}
</li>
{% endfor %}
</ul>