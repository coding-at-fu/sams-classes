""" imported from
https://github.com/sdpython/ensae_teaching_cs/blob/master/src/ensae_teaching_cs/td_1a/flask_helper.py
"""
import threading
from flask import Flask, request

class FlaskInThread (threading.Thread):

  """
  defines a thread for the server
  """

  def __init__(self, app, host="localhost", port=8080):
    """
    constructor
    @param      app     Flask application
    """
    threading.Thread.__init__(self)
    self._app = app
    self._app.add_url_rule(rule='/shutdown', endpoint='shutdown',
      methods=['GET'],view_func=FlaskInThread.shutdown)
    self._host = host
    self._port = port
    self.daemon = True

  def run(self):
    """
    start the server
    """
    self._app.run(host=self._host, port=self._port)

  def shutdown(self):
    """
    shuts down the server, the function could work if:
      * method run keeps a pointer on a server instance
        (the one owning method `serve_forever <https://docs.python.org/3/library/socketserver.html#socketserver.BaseServer.serve_forever>`_)
      * module `werkzeug <http://werkzeug.pocoo.org/>`_ returns this instance
        in function `serving.run_simple <https://github.com/mitsuhiko/werkzeug/blob/master/werkzeug/serving.py>`_
      * module `Flask <http://flask.pocoo.org/>`_ returns this instance in
        method `app.Flask.run <https://github.com/mitsuhiko/flask/blob/master/flask/app.py>`_
    """
    raise NotImplementedError()
    # self.server.shutdown()
    # self.server.server_close()
  
  @staticmethod
  def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
  
  @staticmethod
  def shutdown():
    """ use this api call to shutdown the api server """
    FlaskInThread.shutdown_server()
    return 'Server shutting down...'