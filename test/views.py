""" views.py Funktionen zur verwendung in der Testklasse
"""
from flask import render_template, request

def do_nothing():
  return ""

def hello():
  return render_template('hello.tpl')

def menu_test():
  return render_template('menu.tpl')
