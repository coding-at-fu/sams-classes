import unittest, requests
from sams_classes import SAMSHub, SAMSApp
from sams_classes.exceptions import AppNotExist
from flask import Flask, Blueprint
import sys, time
from .helper_classes import FlaskInThread
import copy

def eprint(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

class CaseSAMSHubWithThreadedAPI(unittest.TestCase):

  def setUp(self):
    self.hub = SAMSHub(
      name = 'test', config = {'default_language': 'de', 'main_app' : 'test'})
    from .api_test_server import app as apiApp
    apiApp.config['TESTING'] = True
    thApi = FlaskInThread(apiApp, host="localhost", port=4711)
    thApi.start()
    time.sleep(1)
 
  def tearDown(self):
    response = requests.get('http://localhost:4711/shutdown')
    print(response.content.decode('utf-8'))
    time.sleep(1)
  
  def test_hub_provides_apps_with_proxies(self):
    manifest = {
      'default_language': 'de',
      'proxies': [{'in': 'api', 'out': 'http://localhost:4711'}]
    }
    self.hub.addApp(
      SAMSApp(name = 'test',manifest = manifest, langDict={'de':{}})
    )
    testApp = SAMSApp(name = 'test.app', manifest = manifest, langDict={'de':{}})
    self.hub.addApp(app = testApp)
    self.hub.addApp(app = testApp, alias = 'test')
    with self.hub.flaskApp.test_client() as client:
      for urlPrefix in ('', '/test.app', '/test'):
        with self.subTest(urlPrefix):
          self.assertIs(client.get(
            path = urlPrefix + '/api/hello').status_code, 200)

class CaseSAMSHubWithMainApp(unittest.TestCase):
  
  def setUp(self):
    self.szenarios = [
      {'main_app': 'test', 'extra_app': 'test.app'},
      {'main_app': 'test.app', 'extra_app': 'test'}
    ] 
  
  def test_main_app(self):
    """It is possible to declare a main app where the urls have no prefix"""
    for szenario in self.szenarios:
      main_app = szenario['main_app']
      extra_app = szenario['extra_app'] 
      with self.subTest(main_app = main_app, extra_app = extra_app):
        hub = SAMSHub(
          name = 'test',
          config = {'default_language': 'de', 'main_app' : ''.join(main_app)}
        )
        expected_names = [
          'Hauptanwendung Punkt 1', 'Hauptanwendung Punkt 2',
          'Zusatzanwendung 1'
        ]
        urls = ['', '2', 'app/1']
        name_vars = ['main_1', 'main_2', 'app_1']
        expected = ''.join((
          "<ul>\n\n<li><a href='/", urls[0], "'>", expected_names[0],
          "</a>\n\n<ul>\n\n<li><a href='/", urls[1], "'>", expected_names[1],
          "</a>\n\n</li>\n\n</ul>\n\n</li>\n\n<li><a href='/", extra_app, '/',
          urls[2], "'>",
        expected_names[2], '</a>\n\n</li>\n\n</ul>'
        ))
        hub.addApp(
          SAMSApp(
            name = main_app,
            manifest = {
              'default_language': 'de',
              'views':[
                {'url': urls[0], 'function': 'views.menu_test'},
                {'url': urls[1], 'function': 'views.do_nothing'}
              ],
              'menu':[
                {
                  'url': urls[0], 'name_string': name_vars[0],
                  'menu': [{'url': urls[1], 'name_string': name_vars[1]}]
                }
              ]
            }
            , langDict = {'de': dict(zip(name_vars, expected_names))}
          )
        )
        hub.addApp(
          SAMSApp(
            name = extra_app,
            manifest = {
              'default_language': 'de',
              'views': [{'url': urls[2], 'function': 'views.do_nothing'}],
              'menu': [{'url': urls[2], 'name_string': name_vars[2]}]
            }, langDict = {'de': dict(zip(name_vars, expected_names))}
          )
        )
        hub.flaskApp.config['DEBUG'] = True
        with hub.flaskApp.test_client(self) as client:
          for path in ('/', '/' + main_app + '/'):
            with self.subTest('check rootpath with content for ' + path):
              response = client.get(path)
              self.assertIs(response.status_code, 200)
              self.assertEquals(response.data.decode('utf-8'), expected)
          for path in ('/' + urls[1], '/' + main_app + '/' + urls[1],
              '/' + extra_app + '/' + urls[2]):
            with self.subTest('check status_code for ' + path):
              response = client.get(path)
              self.assertIs(response.status_code, 200)

  def test_static_files(self):
    for szenario in self.szenarios:
      main_app = szenario['main_app']
      extra_app = szenario['extra_app'] 
      hub = SAMSHub( name = 'test',
        config = {'default_language': 'de', 'main_app' : ''.join(main_app)}
      )
      hub.addApp( 
        SAMSApp( name = main_app, manifest = {'default_language': 'de'},
          langDict = {'de': {}}
        )
      )
      hub.addApp(
        SAMSApp( name = extra_app, manifest = {'default_language': 'de'},
          langDict = {'de': {}}
        )
      )
      hub.flaskApp.config['DEBUG'] = True
      with self.subTest(main_app = main_app, extra_app = extra_app):
        for static_url in ('/test/static/', '/test.app/static/'):
          with hub.flaskApp.test_client(self) as client:
            with self.subTest(static_url):
              response = client.get(static_url + 'test.txt')
              self.assertIs(response.status_code, 200)

class CaseSAMSHubInitExceptions(unittest.TestCase):

  def test_missing_init_name(self):
    with self.assertRaises(TypeError):
      SAMSHub(config= {'default_language': 'de'})
  
  def test_missing_init_config(self):
    with self.assertRaises(TypeError):
      SAMSHub(name = 'test')
  
  def test_missing_init_default_language(self):
    with self.assertRaises(TypeError):
      SAMSHub(name ='test', config = {}) 

class CaseMinimalSAMSHub(unittest.TestCase):
  
  def setUp(self):
    self.hub = SAMSHub(name = 'test', config = {'default_language': 'de'})

  def test_apps_addable(self):
    self.assertIs(len(self.hub.appKeys), 0, 'Precondition, hub has 0 appKeys')
    with self.assertRaises(AppNotExist):
      self.hub.app('test_app')
    self.hub.addApp(
      SAMSApp(
        name = 'test', manifest = {'default_language': 'en'}
        , langDict = {'en':{}}
      )
    )
    self.assertIs(len(self.hub.appKeys), 1, 'Postcondition, hub has 1 appKey')
    self.assertIsInstance(
      self.hub.app('test')
      , SAMSApp, 'Postcondition, "test_app" in hub is a SAMSApp'
    )

  def test_error_apps_add_wrong_type(self):
    with self.assertRaises(TypeError):
      self.hub.addApp('wrong')
  
  def test_has_flask_app(self):
    self.assertIsInstance(self.hub.flaskApp, Flask)
  
  def test_flask_app_readonly(self):
    with self.assertRaises(AttributeError):
      self.hub.flaskApp = Flask('test')
  
  def test_add_app_registers_blueprint(self):
    """The blueprint of the addedApp is registered in flaskApp"""
    self.hub.addApp(
      SAMSApp(
        name = 'test', manifest = {'default_language': 'en'}
        , langDict = {'en':{}}
      )
    )
    self.assertIsInstance(
      self.hub.flaskApp.blueprints.get('test')
      , Blueprint, 'Postcondition, hub.flaskApp has a Blueprint "test"')
  
  def test_context_processor_app_lang(self):
    """The hub sets an app context processor that provides a app_lang dict """
    expected = 'Hallo Welt!'
    self.hub.addApp(
      SAMSApp(
        name = 'test', manifest = {
          'default_language': 'de', 'views':[{
            'url': 'hello', 'function': 'views.hello'}]
        }, langDict = {'de': {'hello': expected}}
      )
    )
    self.hub.flaskApp.config['DEBUG'] = True
    client = self.hub.flaskApp.test_client(self)
    response = client.get('/test/hello')
    self.assertIs(response.status_code, 200)
    self.assertEquals(response.data.decode('utf-8'), expected)

  def test_menu_list_provided(self):
    """The Hub provides a menu list via context_processor"""
    name = 'test'
    expected_names = [
      'Ebene 1: Eintrag 1', 'Eintrag 1 Ebene 2: Eintrag 1', 'Ebene 1: Eintrag 2'
    ]
    urls = ['1/1', '2/1', '1/2']
    name_vars = ['1_1', '2_1', '1_2']
    testSamsApp = SAMSApp(
        name = name, manifest = {
          'default_language': 'de',
          'views': [{'url': 'test-menu','function': 'views.menu_test'}],
          'menu':[
            {'url': urls[0],'name_string': name_vars[0], 'menu':[
              {'url': urls[1],'name_string': name_vars[1]}
            ]},
            { 'url': urls[2], 'name_string': name_vars[2] }
          ]
        }
        , langDict = {'de': dict(zip(name_vars, expected_names))}
    )
    for addAppParams in (
        {'app': testSamsApp}, {'app': testSamsApp, 'alias': 'testAlias'}):
      hub = SAMSHub(name = 'test', config = {'default_language': 'de'})
      effectiveName = addAppParams.get('alias', name)
      expected = ''.join((
        "<ul>\n\n<li><a href='/", effectiveName, '/', urls[0], "'>",
        expected_names[0], "</a>\n\n<ul>\n\n<li><a href='/", effectiveName,
        '/', urls[1], "'>", expected_names[1],
        "</a>\n\n</li>\n\n</ul>\n\n</li>\n\n<li><a href='/", effectiveName, '/',
        urls[2], "'>", expected_names[2], '</a>\n\n</li>\n\n</ul>'
      ))
      hub.addApp(**addAppParams)
      hub.flaskApp.config['DEBUG'] = True
      with hub.flaskApp.test_client(self) as client:
        response = client.get('/' + effectiveName + '/test-menu')
        with self.subTest(addAppParams):
          self.assertIs(response.status_code, 200)
          self.assertEquals(response.data.decode('utf-8'), expected)
  
  def test_hub_default_language_used(self):
    """The hub default_language is prefered instead of app default_language"""
    langDicts = {
      'de': {'hello': 'Hallo Welt!'},
      'en': {'hello': 'Hello world!'}
    }
    for langCode, langDict in langDicts.items():
      with self.subTest(langCode):
        hub = SAMSHub(name = 'test', config = {'default_language': 'de'})
        hub.addApp(
          SAMSApp(
            name = 'test'
            , manifest = {
              'default_language': langCode
              , 'views':[
                {'url': 'hello'
                 , 'function': 'views.hello'}
              ]
            }
            , langDict = copy.deepcopy(langDicts)
          )
        )
        hub.flaskApp.config['DEBUG'] = True
        with hub.flaskApp.test_client(self) as client:
          response = client.get('/test/hello')
          self.assertIs(response.status_code, 200)
          self.assertEquals(
            response.data.decode('utf-8'), langDicts['de']['hello'])

  def test_session_language_used(self):
    """use Session['language'] if possible"""
    langDict = {
      'de': {'hello': 'Hallo Welt!'},
      'en': {'hello': 'Hello world!'},
      'fr': {'hello': 'Bonjour tout le monde!'}
    }
    sessionLangs = ['de', 'en', 'fr']
    hub = SAMSHub(
      name = 'test',
      config = {'default_language': 'de'}
    )
    hub.addApp(
      SAMSApp(
        name = 'test'
        , manifest = {
          'default_language': 'en'
          , 'views':[
            {'url': 'hello'
             , 'function': 'views.hello'}
          ]
        }
        , langDict = copy.deepcopy(langDict)
      )
    )
    for sessionLang in sessionLangs:
      eprint(langDict)
      with self.subTest(sessionLang):
        hub.flaskApp.config['DEBUG'] = True
        with hub.flaskApp.test_client(self) as client:
          with client.session_transaction() as session_simulation:
            session_simulation['language'] = sessionLang
          response = client.get('/test/hello')
          self.assertIs(response.status_code, 200)
          self.assertEquals(
            response.data.decode('utf-8'),langDict[sessionLang]['hello'])

if __name__ == '__main__': unittest.main() 