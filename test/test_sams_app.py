import unittest, time
from sams_classes import SAMSApp, SAMSProxy
import sys
from types import ModuleType
from sams_classes.exceptions import (
  ManifestDefaultLanguageMissing
  , DefaultLanguageDictMissing
)
from flask import Blueprint, Flask
from .helper_classes import FlaskInThread
import requests
import copy

def eprint(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

class TestSAMSApp(unittest.TestCase):
  
  """Class to test SAMSApp"""
  
  def test_has_readonly_name(self):
    """Test, if name attribute of SAMSApp is read only."""
    app_name = 'test_app'
    app = SAMSApp(name = app_name, manifest = {'default_language': 'en'}
      , langDict = {'en':{}})
    self.assertIs(app.name, app_name, 'app.name exists and is correct')
    with self.assertRaises(AttributeError):
      app.name = 'test'
  
  def test_has_blueprint(self):
    """Test, if SAMSApp object has a blueprint attribute of Type Blueprint."""
    app = SAMSApp(name = 'test_app', manifest = {'default_language': 'en'}
      , langDict = {'en':{}})
    self.assertIsInstance(app.blueprint, Blueprint)
  
  def test_has_lang_dict(self):
    """Test, if SAMSApp object has a dict attribute langDict.""" 
    app = SAMSApp(name = 'test_app', manifest = {'default_language': 'en'}
      , langDict = {'en':{}})
    self.assertIsInstance(app.langDict, dict)
  
  def test_default_language_dict_missing(self):
    """Dictionary for specified default_language must be defined on init.""" 
    with self.assertRaises(DefaultLanguageDictMissing):
      app = SAMSApp(name = 'test_app', manifest = {'default_language': 'en'})
  
  def test_manifest_default_language_missing(self):
    """The Manifest has to define a default language."""
    with self.assertRaises(ManifestDefaultLanguageMissing): 
      app = SAMSApp(name = 'test_app')
  
  def test_get_app_module(self):
    """SAMSApp instance has a Module type attribute module"""
    app = SAMSApp(name = 'test', manifest = {'default_language': 'en'}
      , langDict = {'en':{}})
    self.assertIsInstance(app.module, ModuleType)
  
  def test_app_with_view(self):
    """View defined in manifest is reachable with flask app test client."""
    manifest = {
      'default_language': 'en'
      , 'views': [
        {'url': 'test/do_nothing',
          'function': 'views.do_nothing',
          'methods': [
            'GET', 'POST', 'DELETE', 'PUT', 'UPDATE', 'HEAD', 'PATCH',
            'OPTIONS'
          ]
        }, 
        {'url': 'test/do_nothing_defaults', 'function': 'views.do_nothing'}
      ]
    }
    app = SAMSApp(name = 'test', manifest = manifest
      , langDict = {'en':{}})
    flaskApp = Flask(__name__)
    flaskApp.register_blueprint(app.blueprint)
    with self.subTest('test url_map'):
      self.assertGreater(len(list(flaskApp.url_map.iter_rules())), 1)
    thApp = FlaskInThread(flaskApp, host="localhost", port=5000)
    thApp.start()
    time.sleep(0.01)
    for method in manifest['views'][0]['methods']:
      with self.subTest('do_nothing: ' + method):
        response = requests.request(
          method = method, url = 'http://localhost:5000/test/do_nothing')
        self.assertIs(response.status_code, 200)
    for method in ('GET', 'HEAD', 'OPTIONS'):
      # Check that app follows flask 0.6 default behavior for no methods defined
      with self.subTest('do_nothing_default: ' + method):
        response = requests.request(
          method = method,
          url = 'http://localhost:5000/test/do_nothing_defaults')
        self.assertIs(response.status_code, 200)  
    requests.request(
      method = 'GET', url = 'http://localhost:5000/shutdown')
    time.sleep(0.01)

  def test_app_lang(self):
    """App.lang(language) returns dict with requested language / default.""" 
    manifest = {
      'default_language': 'de'
      , 'views': [
        {'url': 'test/do_nothing'
         , 'function': 'views.do_nothing'}
      ]
    }
    default_lang = {
      'overwritten': 'Wird Überschrieben'
      , 'fallback': 'Wird als Fallback verwendet'          
    }
    request_lang = {
      'overwritten': 'Will overwrite default if requested'
    }
    app = SAMSApp(name = 'test', manifest = manifest
      , langDict = {
        'de': copy.deepcopy(default_lang), 'en': copy.deepcopy(request_lang)})
    self.assertEqual(app.lang('en')['overwritten'], request_lang['overwritten'])
    self.assertEqual(app.lang('en')['fallback'], default_lang['fallback'])
    self.assertEqual(app.lang('fr')['overwritten'], default_lang['overwritten'])
  
  def test_app_menu(self):
    """app.menu(language) returns menu entries in specified or default language
    """
    expected_urls = ['/test/1', '/test/2', '/test/2/1', '/test/2/2']
    name_strings = ['test_1', 'test_2', 'test_2_1', 'test_2_2']
    manifest = {
      'default_language': 'de',
      'menu':[
        {'url': copy.copy(expected_urls[0]),
         'name_string': copy.copy(name_strings[0])},
        {'url': copy.copy(expected_urls[1]), 'name_string': copy.copy(name_strings[1]),
         'menu':[
          {'url': copy.copy(expected_urls[2]),
           'name_string': copy.copy(name_strings[2])},
          {'url': copy.copy(expected_urls[3]),
           'name_string': copy.copy(name_strings[3])}
        ]}
      ]
    }
    langDicts = {
      'de': {
        name_strings[0]: '1 überschrieben',
        name_strings[1]: '2 verwendet',
        name_strings[2]: '2.1 verwendet',
        name_strings[3]: '2.2 überschrieben'          
      },
      'en': {
        name_strings[0]: '1 used',
        name_strings[3]: '2.2 used'
      }
    }
    app = SAMSApp(name = 'test', manifest = manifest
      , langDict = copy.deepcopy(langDicts))
    menu = app.menu('en')
    self.assertEqual(menu[0]['url'], expected_urls[0])
    self.assertEqual(menu[1]['url'], expected_urls[1])
    self.assertEqual(menu[1]['menu'][0]['url'], expected_urls[2])
    self.assertEqual(menu[1]['menu'][1]['url'], expected_urls[3])
    self.assertEqual(menu[1]['name'], langDicts['de'][name_strings[1]])
    self.assertEqual(menu[0]['name'], langDicts['en'][name_strings[0]])
    self.assertEqual(menu[1]['menu'][1]['name'], langDicts['en'][name_strings[3]])
    self.assertEqual(menu[1]['menu'][0]['name'], langDicts['de'][name_strings[2]])
    menu = app.menu('de')
    self.assertEqual(menu[0]['name'], langDicts['de'][name_strings[0]])
    self.assertEqual(menu[1]['menu'][1]['name'], langDicts['de'][name_strings[3]])

  def test_app_menu_prefix(self):
    """app.menu(lanCode, prefix) returns the app menu with prefixed urls"""
    manifest = {
      'default_language': 'en', 'menu': [
        {'url': '', 'name_string': '0'}, {'url': '1', 'name_string': '1'}
      ]
    }
    langDict = {'0': 'main', '1': 'second'}
    app = SAMSApp(name = 'test', manifest = manifest
      , langDict = {'en': langDict})
    menu = app.menu(langCode = 'en', urlPrefix = '/test')
    self.assertEqual(menu[0]['url'], '/test/')
    self.assertEqual(menu[1]['url'], '/test/1')
  
  def test_app_menu_external_urls(self):
    """app.menu(lanCode, prefix) ignors external urls for prefix"""
    manifest = {
      'default_language': 'en', 'menu': [
        {'url': '', 'name_string': '0'},
        {'url': 'http://zib.de', 'name_string': '1', 'external': True}
      ]
    }
    langDict = {'0': 'main', '1': 'second'}
    app = SAMSApp(name = 'test', manifest = manifest
      , langDict = {'en': langDict})
    menu = app.menu(langCode = 'en', urlPrefix = '/test')
    self.assertEqual(menu[0]['url'], '/test/')
    self.assertEqual(menu[1]['url'], 'http://zib.de')
  
  def test_app_proxies_list(self):
    """ app.proxies provides list even if no proxy is defined in manifest"""
    app = SAMSApp(name = 'test', manifest = {'default_language': 'de'},
      langDict = {'de': {}})
    self.assertIsInstance(app.proxies, list)
  
  def test_app_proxies_elements_type_sams_proxy(self):
    """ app.proxies contains elements of Type SAMSProxy """
    app = SAMSApp(name = 'test', langDict = {'de': {}},
      manifest = {'default_language': 'de', 'proxies': [
        {'in': 'api/user', 'out': 'https//my.api.tld'},
        {'in': 'api/group', 'out': 'https//my.api2.tld'}]
      }
    )
    self.assertIs(len(app.proxies), len(app.manifest.get('proxies')))
    for element in app.proxies:
      self.assertIsInstance(element, SAMSProxy)

class TestSAMSAppWithThreadedApi(unittest.TestCase):
  """Class to test SAMSApp with a threaded API App as backend
  
  This TestCase provides a setup and tearDown for a API App in a separate
  thread which is reachable at http://localhost:4711.
  """
  
  def setUp(self):
    from .api_test_server import app as apiApp
    thApi = FlaskInThread(apiApp, host="localhost", port=4711)
    thApi.start()
    time.sleep(0.01)
    self.flaskApp = Flask('test')
    self.flaskApp.config['TESTING'] = True

  def tearDown(self):
    response = requests.get('http://localhost:4711/shutdown')
    print(response.content.decode('utf-8'))
    time.sleep(0.01)

  def test_app_proxy_registered(self):
    """SAMSApp registers the needed url rue for proxy in its bluebprint"""
    testApp = SAMSApp(name = 'test', langDict = {'de': {}},
      manifest = {'default_language': 'de', 'proxies': [
        {'in': 'api', 'out': 'http://localhost:4711'}]
      }
    )
    self.flaskApp.register_blueprint(testApp.blueprint)
    thApp = FlaskInThread(self.flaskApp, host="localhost", port=5000)
    thApp.start()
    time.sleep(0.01)
    with self.subTest('test flask url_map'):
      self.assertGreater(len(list(self.flaskApp.url_map.iter_rules())), 1)
    for path in ('/api/hello', '/api/'):
      for method in ( 'GET', 'POST', 'DELETE', 'UPDATE', 'PATCH', 'OPTIONS',
          'PUT', 'HEAD'):
        with self.subTest('test path ' + path + ' method ' + method):
          response = requests.request(method = method,
            url = 'http://localhost:5000' + path)
          self.assertIs(response.status_code, 200)
    requests.get('http://localhost:5000/shutdown')
    

if __name__ == '__main__':
    unittest.main()