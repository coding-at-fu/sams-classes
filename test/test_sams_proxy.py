import unittest, time, json 
from sams_classes import SAMSProxy
from sams_classes.exceptions import (
  ProxySpecKeyMssing, ProxySpecMissing
)
from flask import Flask
import sys, requests
from .helper_classes import FlaskInThread

def eprint(*args, **kwargs):
  print(*args, file=sys.stderr, **kwargs)

class TestSAMSHub(unittest.TestCase):

  def test_proxy_spec_mandatory (self):
    """ SAMSProxy needs a proxy_spec dict, raies error if not supplied"""
    with self.assertRaises(TypeError):
      SAMSProxy()
    with self.assertRaises(ProxySpecMissing):
      SAMSProxy(proxySpec = {})

  def test_proxy_spec_musst_be_dict(self):
    """ SAMSProxy raies an TypeExecption if ProxySpec is no dict"""
    with self.assertRaises(TypeError):
      SAMSProxy(proxySpec = 'wrong')

  def test_mandatory_keys_in_spec(self):
    """ ProxySpecKeyMissing is raised if something is mising in spec"""
    proxySpecs = [
      {'out': 'http://baz.foo'},
      {'in': '/foo/bar'},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'token': {'name': 'user'}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'token': {'value': 'user'}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'token': {'name': 'user', 'value': 'user'}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'session-passthrough': {}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'session-passthrough': {'default': {}}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'session-passthrough': {'default': {'user': {'name': 'u'}}}},
      {'in': '/foo/bar', 'out': 'http://baz.foo'
        , 'session-passthrough': {'default': {'user': {'param_type': 'body'}}}}
    ]
    for method in ('get', 'post', 'delete', 'put', 'update'):
      proxySpecs.append({'in': '/foo/bar', 'out': 'http://baz.foo',
        'session-passthrough': {'default': {
          'user': {'param_type': 'body', 'name': 'u'}
        }, method: {'user': {'param_type': 'body'}}}})
      proxySpecs.append({'in': '/foo/bar', 'out': 'http://baz.foo',
        'session-passthrough': {'default': {
          'user': {'param_type': 'body', 'name': 'u'}
        }, method: {'user': {'name': 'u'}}}})
    for proxySpec in proxySpecs:
      with self.subTest(proxySpec):
        with self.assertRaises(ProxySpecKeyMssing):
          SAMSProxy(proxySpec = proxySpec)

class TestSAMSHubWithThreadedAPI(unittest.TestCase):

  def setUp(self):
    from .api_test_server import app as apiApp
    apiApp.config['TESTING'] = True
    thApi = FlaskInThread(apiApp, host="localhost", port=4711)
    thApi.start()
    time.sleep(0.01)
    self.proxyApp = Flask('test')
    self.proxyApp.config['TESTING'] = True
    self.basic_url_rule_params = {'endpoint': 'proxy',
      'methods': ['GET', 'POST', 'PUT', 'UPDATE', 'PATCH', 'DELETE', 'OPTIONS']}
    self.url_rule_properties = ['rootUrlRule', 'urlRule'] 
 
  def tearDown(self):
    response = requests.get('http://localhost:4711/shutdown')
    print(response.content.decode('utf-8'))
    time.sleep(0.01)

  def test_json_passthrough(self):
    """The proxy passes json data """
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711'})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    thProxyApp = FlaskInThread(self.proxyApp, host="localhost", port=5000)
    thProxyApp.start()
    time.sleep(0.01)
    res = requests.post(url='http://localhost:5000/proxy/passthrough',
      json={'g':'bar'})
    resDict = json.loads(res.content.decode('utf-8'))
    self.assertEqual(resDict['body']['g'], 'bar')
    requests.get('http://localhost:5000/shutdown')
    time.sleep(0.01)

  def test_session_wins(self):
    """proxy guaranties that session params will overwrite the request params"""
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711',
        'session_passthrough': {'default': {
          'user': {'name': 'u','param-type': 'url'},
          'group': {'name': 'g', 'param-type': 'body'},
          'language': {'name': 'Lang', 'param-type': 'header'}}}})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    self.proxyApp.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    with self.proxyApp.test_client(self) as client:
      with client.session_transaction() as sess:
        sess['user'] = 'foo'
        sess['group'] = 'bar'
        sess['language'] = 'klingon'
      res = client.get(path='/proxy/passthrough', query_string={'u':'quin'},
        data={'g':'admins'}, headers={'Lang': 'romulan'})
      resDict = json.loads(res.data.decode('utf-8'))
      self.assertEqual(resDict['url']['u'], sess['user'])
      self.assertEqual(resDict['body']['g'], sess['group'])
      self.assertEqual(resDict['header']['Lang'], sess['language'])

  def test_session_param_passthrough(self):
    """The proxy passes the session and the normal params"""
    self.proxyApp = Flask('test')
    self.proxyApp.config['TESTING'] = True
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711',
        'session_passthrough': {'default': {
          'user': {'name': 'u','param-type': 'url'},
          'group': {'name': 'g', 'param-type': 'body'},
          'language': {'name': 'lang', 'param-type': 'header'}}}})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    self.proxyApp.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    with self.proxyApp.test_client(self) as client:
      with client.session_transaction() as sess:
        sess['user'] = 'foo'
        sess['group'] = 'bar'
        sess['language'] = 'klingon'
      res = client.get(path='/proxy/passthrough', query_string={'q':'Query'},
        data={'f':'Form'}, headers={'Some-Mode': 'whatever'})
      resDict = json.loads(res.data.decode('utf-8'))
      self.assertEqual(resDict['url']['u'], sess['user'])
      self.assertEqual(resDict['body']['g'], sess['group'])
      self.assertEqual(resDict['header']['Lang'], sess['language'])
      self.assertEqual(resDict['url']['q'], 'Query')
      self.assertEqual(resDict['body']['f'], 'Form')
      self.assertEqual(resDict['header']['Some-Mode'], 'whatever')

  def test_session_passthrough(self):
    """The proxy passes the specified session param as specified param_type"""
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711',
        'session_passthrough': {'default': {
          'user': {'name': 'u','param-type': 'url'},
          'group': {'name': 'g', 'param-type': 'body'},
          'language': {'name': 'lang', 'param-type': 'header'}}}})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    self.proxyApp.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    with self.proxyApp.test_client(self) as client:
      with client.session_transaction() as sess:
        sess['user'] = 'foo'
        sess['group'] = 'bar'
        sess['language'] = 'klingon'
      res = client.get(path='/proxy/passthrough')
      resDict = json.loads(res.data.decode('utf-8'))
      self.assertEqual(resDict['url']['u'], sess['user'])
      self.assertEqual(resDict['body']['g'], sess['group'])
      self.assertEqual(resDict['header']['Lang'], sess['language'])
  
  def test_simple_proxy_forwarding(self):
    """ The proxy forwards a request to test flaskapp and returns 'hello' """
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711'})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    with self.proxyApp.test_client(self) as client:
      eprint(self.proxyApp.url_map)
      for method in ['get', 'post', 'put', 'update', 'patch', 'delete',
          'options']:
        for path in ('/proxy/hello', '/proxy/'):
          response = client.open(path = path, method = method.upper())
          with self.subTest('method ' + method + ' path ' + path):
            self.assertIs(response.status_code, 200, method)

  def test_param_passthrough(self):
    """The proxy passes headers, form / data and url params """
    proxy = SAMSProxy(
      proxySpec = {'in': '/proxy', 'out': 'http://localhost:4711'})
    for urlRuleProp in self.url_rule_properties:
      self.proxyApp.add_url_rule(rule = getattr(proxy, urlRuleProp),
        view_func = proxy.proxy, **self.basic_url_rule_params)
    with self.proxyApp.test_client(self) as client:
      res = client.get(path='/proxy/passthrough',
        query_string={'u':'foo'}, data={'g':'bar'}, headers={'lang': 'klingon'})
      resDict = json.loads(res.data.decode('utf-8'))
      self.assertEqual(resDict['url']['u'], 'foo')
      self.assertEqual(resDict['body']['g'], 'bar')
      self.assertEqual(resDict['header']['Lang'], 'klingon')
  
  def test_token_passthrough(self):
    """The proxy passes the specified token and the other params"""
    for param_type in ('header', 'url', 'body'):
      proxyApp = Flask('test')
      proxyApp.config['TESTING'] = True
      proxyApp.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
      proxy = SAMSProxy(
        proxySpec = {
          'in': '/proxy', 'out': 'http://localhost:4711',
          'token': {
            'name': 'Api-Token', 'value': '53CUR34P170K3N',
            'param-type': param_type
          }
        }
      )
      with self.subTest(param_type):
        proxyApp.add_url_rule(rule = proxy.urlRule, endpoint = 'proxy',
          view_func = proxy.proxy, methods=['GET', 'POST', 'PUT', 'UPDATE',
            'PATCH', 'DELETE', 'OPTIONS'])
        proxyApp.add_url_rule(rule = proxy.rootUrlRule, endpoint = 'proxy',
          view_func = proxy.proxy, methods=['GET', 'POST', 'PUT', 'UPDATE',
            'PATCH', 'DELETE', 'OPTIONS'])
        with proxyApp.test_client(self) as client:
          with client.session_transaction() as sess:
            sess['user'] = 'foo'
          res = client.get(path='/proxy/passthrough',
            query_string={'u':'foo'}, data={'g':'bar'},
            headers={'lang': 'klingon'})
          resDict = json.loads(res.data.decode('utf-8'))
          self.assertEqual(resDict['url']['u'], 'foo')
          self.assertEqual(resDict['body']['g'], 'bar')
          self.assertEqual(resDict['header']['Lang'], 'klingon')
          eprint(resDict[param_type]) 
          self.assertEquals(resDict[param_type]['Api-Token'], '53CUR34P170K3N')

  def test_no_token_passthrough_without_session(self):
    """The proxy passes the specified token and the other params"""
    for param_type in ('header', 'url', 'body'):
      proxyApp = Flask('test')
      proxyApp.config['TESTING'] = True
      proxyApp.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
      proxy = SAMSProxy(
        proxySpec = {
          'in': '/proxy', 'out': 'http://localhost:4711',
          'token': {
            'name': 'Api-Token', 'value': '53CUR34P170K3N',
            'param-type': param_type
          }
        }
      )
      with self.subTest(param_type):
        proxyApp.add_url_rule(rule = proxy.urlRule, endpoint = 'proxy',
          view_func = proxy.proxy, methods=['GET', 'POST', 'PUT', 'UPDATE',
            'PATCH', 'DELETE', 'OPTIONS'])
        proxyApp.add_url_rule(rule = proxy.rootUrlRule, endpoint = 'proxy',
          view_func = proxy.proxy, methods=['GET', 'POST', 'PUT', 'UPDATE',
            'PATCH', 'DELETE', 'OPTIONS'])
        with proxyApp.test_client(self) as client:
          res = client.get(path='/proxy/passthrough',
            query_string={'u':'foo'}, data={'g':'bar'},
            headers={'lang': 'klingon'})
          resDict = json.loads(res.data.decode('utf-8'))
          self.assertEqual(resDict['url']['u'], 'foo')
          self.assertEqual(resDict['body']['g'], 'bar')
          self.assertEqual(resDict['header']['Lang'], 'klingon')
          eprint(resDict[param_type]) 
          self.assertNotEquals(
            resDict.get(param_type, {}).get('Api-Token'), '53CUR34P170K3N')